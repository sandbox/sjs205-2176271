<?php


require_once ( dirname(__FILE__) . '/sensors.helper.inc');

/** 
 * implementation of hook_block_view()
 * see 
 * http://drupal.stackexchange.com/questions/46227/creating-multiple-blocks-programmatically
 for details of multiple blocks
 */
function sensors_block_view($delta = '') {
  $block = array();
  switch($delta) {
    /*case 'current_reading_chart':
      $block = array(
          'subject' => t(''),
          'content' => current_reading_chart_block(),
          );
      break;
    case 'last24h_reading_chart':
      $block = array(
          'subject' => t(''),
          'content' => last24h_reading_chart_block(),
          );
      break;
*/

  }
  return $block;

}
/*
 * Function to display sensor stats 
 */
function sensor_stats_block($sensor) {

  $block=array();

  $block['sensor_description'] = array(
      '#markup' => '<h4><b>Sensor:</b> ' . $sensor['name'] . ' Stats</h4>'
      . '<h4><b>Description:</b> ' . $sensor['description'] . '</h4>'
      );

  $block['sensor_type'] = array(
      '#markup' => '<h4><b>Sensor Type:</b> ' . $sensor['type_desc'] . '</h4>'
      . '<h4><b>Sensor Measurement (Units):</b> ' . $sensor['type'] 
      . ' (' . $sensor['units'] . ')</h4>'
      );

  $block['sensor_device'] = array(
      '#markup' => '<h4><b>Sensor Device:</b> <a href="?q=sensors_client&deviceID=' 
      . $sensor['deviceID'] . '">' . $sensor['deviceID'] . '</a></h4>'
      );

  return($block);
}
/*
 * Function to display profile stats 
 */
function profile_stats_block($profile) {

  /* format interval */
  if($profile['interval'] <= 60) {
    $interval = $profile['interval'];
    $units = 'minutes';
  } elseif($profile['interval'] > 60 && $profile['interval'] <= 2880) {
    $interval = round($profile['interval']/60,2);
    $units = 'hours';
  } else {
    $interval = round($profile['interval']/1440,2);
    $units = 'days';
  }
  $block=array();


  $block['profile_description'] = array(
      '#markup' => '<h4><b>Profile:</b> ' . $profile['name'] . '</h4>'
      . '<h4><b>Description:</b> ' . $profile['description'] . '</h4>'
      );

  $block['profile_dates'] = array(
      '#markup' => '<h4><b>Start Date:</b> ' . $profile['date'] . ' </h4>'
      . '<h4><b>End Date:</b> ' 
      . (isset($profile['end_date']) ? $profile['end_date'] : 'Not Set') 
      . ' </h4>'
      . '<h4><b>Interval:</b> ' . $interval . ' ' . $units . '</h4>'
      );
  $block['profile_max_min'] = array(
      '#markup' => '<h4><b>Max:</b>' . $profile['maxmin']['max']['reading'] 
      . ' ' . $profile['sensor']['units']  . ' <b>Min:</b> ' 
      . $profile['maxmin']['min']['reading'] . ' ' . $profile['sensor']['units'] . '</h4>'
      );

  /*$block['profile_fieldset']['reading_count'] = array(
      '#markup' => '<h4><b>No. of Readings:</b> ' . 'XXXXXXXX ' . '</h4>'
      );
*/
  return($block);
}


/*
 * Function to return no data available message
 */
function no_data_available_block($sensorID) {
  if($sensorID != 0 && $sensorID != NULL) {
    $block['no_data_available'] = array(
        '#prefix' => '<div class="no_data">',
        '#markup' => '<h4>' . t('No data is currently available for sensor:') . $sensorID,
        '#suffix' => '</div>',
        );
  } else {
    $block['no_sensor_available'] = array(
        '#prefix' => '<div class="no_data">',
        '#markup' => '<h4>' 
        . t('No sensor is currently available, Please check you configuration.'),
        '#suffix' => '</div>',
        );
  }
  return($block);
}

function devices_list_block($select = NULL, $link_url = NULL) {

  $block = array();
  $devices_db = sensors_get_devices_db();

  if($link_url == NULL) 
    $link_url = 'sensors_device';


  $header = array(
      'deviceID' => t('Device ID'),
      'name' => t('Device Name'),
      'description' => t('Device Description'),
      );
  $rows=array();
  foreach($devices_db as $device) {
    $row=array();
    $row['deviceID'] = '<a href="?q=' . $link_url . '&deviceID=' 
      . $device->deviceID . '">' . $device->deviceID . '</a>' ;
    $row['name'] = '<a href="?q=' . $link_url . '&deviceID=' 
      . $device->deviceID . '">' . $device->name . '</a>' ;
    $row['description'] = '<a href="?q=' . $link_url . '&deviceID=' 
      . $device->deviceID . '">' . $device->description . '</a>' ;
    $rows[$device->deviceID] = $row;
  }

  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['devices_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No Devices are available. <a href="?q=admin/config/sensors_devices">Please add a device </a>'),
        );
  } else {
    $block['devices_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No Devices are available. <a href="?q=admin/config/sensors_devices">Please add a device </a>'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}
/* 
 * Function to return a list of sensor types
 */
function sensor_types_list_block($select = NULL, $link_url = NULL) {

  $block = array();
  $types_db = sensors_get_sensor_types_db();

  if($link_url == NULL) 
    $link_url = 'sensor_types';


  $header = array(
      'typeID' => t('Type ID'),
      'type' => t('Sensor Type'),
      'units' => t('Type Units'),
      'description' => t('Type Description'),
      );
  $rows=array();
  if($types_db != NULL) { 
    foreach($types_db as $sensor_type) {
      $row=array();
      $row['typeID'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $sensor_type->typeID . '">' . $sensor_type->typeID . '</a>' ;
      $row['type'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $sensor_type->typeID . '">' . $sensor_type->type . '</a>' ;
      $row['units'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $sensor_type->typeID . '">' . $sensor_type->units . '</a>' ;
      $row['description'] = '<a href="?q=' . $link_url . '&typeID=' 
        . $sensor_type->typeID . '">' . $sensor_type->description . '</a>' ;
      $rows[$sensor_type->typeID] = $row;
    }
  }
  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['types_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No sensor types are available. <a href="?q=admin/config/sensor_types">Please add a new sensor type </a>'),
        );
  } else {
    $block['types_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No sensor types are available. <a href="?q=admin/config/sensors_types">Please add a new sensor type </a>'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}


function sensors_list_block($deviceID = NULL, $sensorID = NULL, $select = NULL, $link_url = NULL) {
  $block = array();
  $header = array(
      'sensorID' => t('sensor ID'),
      'name' => t('Name'),
      'description' => t('Description'),
      'type' => t('Measurement Type'),
      'units' => t('Units'),
      'hardwareID' => t('Hardware ID'),
      'device' => t('Device connected to'),
      );

    $sensors = get_sensors($deviceID, $sensorID);

  if(!empty($sensors)) {
    $rows=array();
    foreach($sensors as $sensor) {
      $row=array();
      $row['sensorID'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['sensorID'] . '</a>' ;
      $row['name'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['name'] . '</a>' ;
      $row['description'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['description'] . '</a>' ;
      $row['type'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['type'] . '</a>' ;
      $row['units'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['units'] . '</a>' ;
      $row['hardwareID'] = '<a href="?q=' . ($link_url ? $link_url : 'sensors') . '&sensorID=' 
        . $sensor['sensorID'] . '">' . $sensor['hardwareID'] . '</a>' ;
      $row['device'] = '<a href="?q=' . ($link_url ? $link_url : 'device') . '&deviceID=' 
        . $sensor['deviceID'] . '">' . $sensor['deviceID'] . ':' 
        . $sensor['device_name'] . '</a>' ;
      $rows[$sensor['sensorID']] = $row;
    }
  }
  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No Sensors are available for the selected device.'),
        );
  } else {
    $block['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No Sensors are available for the selected device.'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}

function device_block($deviceID) {

  $block = array();

  $device = sensors_get_device_db($deviceID);
  $block['fieldset_device'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="device_fieldset">',
      '#suffix' => '</div>',
      '#title' => $device['name'] . ' - ' . $device['description'],
      );

  $block['fieldset_device']['device_data'] = array(
      '#markup' => '<h4><b>DeviceID:</b> ' . $device['deviceID'] . '</h4>' 
      . '<h5><b>Software Version:</b> ' . $device['sver'] . '</h5>',
      );

  return $block;
}

function sensor_block($sensorID) {

  $block = array();

  $sensor = sensors_get_sensor_db($sensorID);
  $block['fieldset_sensor'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensor_fieldset">',
      '#suffix' => '</div>',
      '#title' => $sensor['name'] . ' - ' . $sensor['description'],
      );

  $block['fieldset_sensor']['sensor_data'] = array(
      '#markup' => '<h4><b>SensorID:</b> ' . $sensor['sensorID'] . '</h4>' 
      . '<h5><b>Sensor Type:</b> ' . $sensor['type'] . '</h5>'
      . '<h5><b>Sensor Units:</b> ' . $sensor['units'] . '</h5>'
      . '<h5><b>HardwareID:</b> ' . $sensor['hardwareID'] . '</h5>',
      );

  return $block;
}

function sensor_stat_block($sensorID) {

  $block = array();

  $sensor = sensors_get_sensor_db($sensorID);
  $block['fieldset_sensor_stat'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensor_stat_fieldset">',
      '#suffix' => '</div>',
      '#title' => $sensor['name'] . ' - Stats',
      );

  $block['fieldset_sensor_stat']['sensor_data'] = array(
      '#markup' => '<h4><b>Reading count:</b></h4>' 
      . '<h5><b> last hour:</b> ' . 72 . '</h5>'
      . '<h5><b> Today:</b> ' . 2 . '</h5>'
      . '<h5><b> Yesterday:</b> ' . 3000 . '</h5>',
      );

  return $block;
}

function sensor_readings_list_block($sensorID, $select = NULL, $link_url = NULL) {

  $block = array();
  $header = array(
      'readingID' => t('ReadingID'),
      'date' => t('Reading Date'),
      'type' => t('Measurement'),
      );
  

    $readings = sensors_get_sensor_readings_db($sensorID);

  if(!empty($readings)) {
    $rows=array();
    foreach($readings as $reading) {
      $row=array();
      $row['readingID'] = '<a href="?q=' . ($link_url ? $link_url : 'readings') . '&readingID=' 
        . $reading->readingID . '">' . $reading->readingID . '</a>' ;
      $row['date'] = '<a href="?q=' . ($link_url ? $link_url : 'readings') . '&readingID=' 
        . $reading->readingID . '">' . $reading->date . '</a>' ;
      $row['measurement'] = '<a href="?q='. ($link_url ? $link_url : 'readings') . '&readingID=' 
        . $reading->readingID . '">' . $reading->measurement . '</a>' ;
      $rows[$reading->readingID] = $row;
    }
  }

  $block['fieldset_readings'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="readings_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Sensor Readings'),
      );
  $block['fieldset_readings']['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['fieldset_readings']['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No readings are available for the selected sensor.'),
        );
  } else {
    $block['fieldset_readings']['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No readings are available for the selected sensor.'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}

function sensor_calcs_list_block($sensorID, $select = NULL, $link_url = NULL) {

  $block = array();
  $header = array(
      'calc_type' => t('Types'),
      'interval' => t('Interval'),
      'date' => t('From'),
      'expires' => t('To'),
      'result' => t('Result'),
      );
  

    $calcs = sensors_get_sensor_calcs_db($sensorID);

  if(!empty($calcs)) {
    $rows=array();
    $rowID = 0;
    foreach($calcs as $calc) {
      $row=array();
      $row['calc_type'] = '<a href="?q=' . ($link_url ? $link_url : 'calcs') . '&calcID=' 
        . $calc->calc_type . '">' . $calc->calc_type . '</a>' ;
      $row['interval'] = '<a href="?q=' . ($link_url ? $link_url : 'calcs') . '&calcID=' 
        . $calc->calc_type . '">' . $calc->time_interval . '</a>' ;
      $row['date'] = '<a href="?q=' . ($link_url ? $link_url : 'calcs') . '&calcID=' 
        . $calc->calc_type . '">' . $calc->date . '</a>' ;
      $row['expires'] = '<a href="?q='. ($link_url ? $link_url : 'calcs') . '&calcID=' 
        . $calc->calc_type . '">' . $calc->expires . '</a>' ;
      $row['result'] = '<a href="?q='. ($link_url ? $link_url : 'calcs') . '&calcID=' 
        . $calc->calc_type . '">' . $calc->result . '</a>' ;
      $rows[$rowID++] = $row;
    }
  }

  $block['fieldset_calcs'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="calcs_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Sensor Readings'),
      );
  $block['fieldset_calcs']['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['fieldset_calcs']['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No calcs are available for the selected sensor.'),
        );
  } else {
    $block['fieldset_calcs']['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No calcs are available for the selected sensor.'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}

function device_readings_list_block($deviceID, $select = NULL, $link_url = NULL) {

  $block = array();
  $header = array(
      'readingID' => t('ReadingID'),
      'date' => t('Reading Date'),
      'type' => t('Measurements'),
      );

    $readings = device_get_sensors_readings_db($deviceID);

    if(!empty($readings)) {
      $rows=array();
      foreach($readings as $reading) {
        $row=array();
        $rows[$reading->readingID]['readingID'] = '<a href="?q=' . ($link_url ? $link_url : 'readings') . '&readingID=' 
          . $reading->readingID . '">' . $reading->readingID . '</a>' ;
        $rows[$reading->readingID]['date'] = '<a href="?q=' . ($link_url ? $link_url : 'readings') . '&readingID=' 
          . $reading->readingID . '">' . $reading->date . '</a>' ;
        $row['measurement'] = '<a href="?q='. ($link_url ? $link_url : 'readings') . '&readingID=' 
          . $reading->readingID . '">' . $reading->description . ': ' . $reading->measurement . ' ' .  $reading->units
          . '</a><br \>' ;

        if (isset($rows[$reading->readingID]['measurement']))
        {
          $rows[$reading->readingID]['measurement'] .= $row['measurement'];
        }
        else
        {
          $rows[$reading->readingID]['measurement'] = $row['measurement'];
        }
      }
    }

  $block['fieldset_readings'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="readings_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Device Readings'),
      );
  $block['fieldset_readings']['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['fieldset_readings']['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No readings are available for the selected device.'),
        );
  } else {
    $block['fieldset_readings']['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => (!empty($rows) ? $rows : NULL),
        '#empty' => t('No readings are available for the selected device.'),
        '#multiple' => FALSE,
        '#default_value' => $select,
        );
  }
  return $block;
}

function profile_list_block($deviceID = NULL, $sensorID = NULL, $select = NULL) {

  $block = array();
  $profiles_db = sensors_get_devices_profiles_db($deviceID, $sensorID);

  $header = array(
      'name' => t('Profile Name'),
      'description' => t('Profile Description'),
      's_name' => t('Sensor ID:Name'),
      's_description' => t('Sensor Description'),
      'type' => t('Sensor Type'),
      'units' => t('Units'),
      'date' => t('Profile Start Date'),
      'end_date' => t('Profile End Date'),
      );
  $rows=array();
  foreach($profiles_db as $profile) {
    $row=array();
    $row['name'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->p_name . '</a>' ;
    $row['description'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->p_description . '</a>' ;
    $row['s_name'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->sensorID . ':' .$profile->name . '</a>' ;
    $row['s_description'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->description . '</a>' ;
    $row['type'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->type . '</a>' ;
    $row['units'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->units . '</a>' ;
    $row['date'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->date . '</a>' ;
    $row['end_date'] = '<a href="?q=sensors_profile&profileID=' 
      . $profile->profileID . '">' . $profile->end_date . '</a>' ;
    $rows[$profile->profileID] = $row;
  }


  $block['fieldset_profiles'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensor_profiles">',
      '#suffix' => '</div>',
      '#title' => t('Sensor Profiles'),
      );

  $block['fieldset_profiles']['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['fieldset_profiles']['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#default_value' => $select,
        '#empty' => t('No Sensors are available for the selected device.'),
        );
  } else {
    $block['fieldset_profiles']['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => 
          t('No profiles are available for the selected device or sensor.'),
        '#multiple' => FALSE,
        );
  }
  return $block;
}

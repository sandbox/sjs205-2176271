<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_devices_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_devices_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_devices_my_form($form_state) {


  $form_state['values']['deviceID'] = get_deviceID();
  $form_state['values']['sensorID'] = get_sensorID();

  if($form_state['values']['deviceID'] != NULL) {
    $device = sensors_get_device_db($form_state['values']['deviceID']);
  }

  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );

  if($form_state['values']['deviceID'] != NULL) {

  $form['device'] = device_block($form_state['values']['deviceID']);

  $form['device_readings'] = device_readings_list_block($form_state['values']['deviceID']);

    $form['fieldset_sensors'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="sensors_fieldset">',
        '#suffix' => '</div>',
        '#title' => t('Sensors Attached to Device'),
        );


    $form['fieldset_sensors']['sensors'] = sensors_list_block($form_state['values']['deviceID']);

  } else {
  $form['fieldset_devices'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="devices_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Devices'),
      );


  $form['fieldset_devices']['devices'] = devices_list_block(NULL, 'sensors_devices');
  }
  $form['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  $form['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );

  $form['row_container_2_end'] = array(
      '#markup' => '</div>'
      );



  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_devices_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_devices_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_devices_admin($form_state) {


  $form_state['values']['deviceID'] = get_deviceID();

  $update = FALSE;
  if($form_state['values']['deviceID'] != NULL) {
    /* UPDATE query */
    $update = TRUE;
    $device = sensors_get_device_db($form_state['values']['deviceID']);
  }

  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );

  $form['fieldset_devices'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="devices_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Devices'),
      );


  $form['fieldset_devices']['devices'] = devices_list_block(NULL, 'admin/config/sensors_devices');
  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Create New Device'),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  $form['fieldset_create']['device'] = array(
      '#markup' => '<p>' .
      ($update 
       ? t('Update device details below, once complete, press \'Submit\'.') 
       : t('Enter details of the new device to be added below, once complete, press Submit.') 
      ) . '</p>',
      );

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['device_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_device_name_desc">',
      '#suffix' => '</div>',
      );
  if($update) {
    $form['fieldset_create']['device_name_desc']['deviceID'] = array(
        '#type' => 'textfield',
        '#title' => t('DeviceID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $device['deviceID'],
        '#attributes' => array('disabled' => 'TRUE'),
        );
  }
  $form['fieldset_create']['device_name_desc']['device_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Device Name'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $device['name'] : NULL ),
      );
  $form['fieldset_create']['device_name_desc']['device_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Device Description'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $device['description'] : NULL ),
      );

  $form['fieldset_create']['device_name_desc']['device_sver'] = array(
      '#type' => 'textfield',
      '#title' => t('Device Software Version'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $device['sver'] : NULL ),
      );
  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => 'Submit',
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_devices_my_form_submit'),
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

function sensors_devices_my_form_submit($form, &$form_state) {

  if(isset($form_state['values']['deviceID'])) {
    /* UPDATE existing Device */
    if(!(isset($form_state['values']['device_name']) &&
          isset($form_state['values']['device_description']))) {
      drupal_set_message("You must specify at least 'Device Name', 'Device Description'"
          . " to update a device.", 'warning');
    } else {
      $tmp = sensors_update_device_db(
          $form_state['values']['deviceID'],
          $form_state['values']['device_name'],
          $form_state['values']['device_description'],
          $form_state['values']['device_sver']);

      if($tmp < 0) {
        drupal_set_message(t('Your device could not be updated...'), 'warning');
      } else {
        drupal_set_message(t('Your device has been updated.'), 'status');
      }
    }
  } else {
    $tmp = 0;

    if(!(isset($form_state['values']['device_name']) &&
          isset($form_state['values']['device_description']))) {
      drupal_set_message("You must specify at least 'Device Name', 'Device Description'"
          . " to create a new device.", 'warning');
    } else {
      $tmp = sensors_insert_device_db(
          $form_state['values']['device_name'],
          $form_state['values']['device_description'],
          $form_state['values']['device_sver']);

      if($tmp < 0) {
        drupal_set_message(t('New device could not be created...'), 'warning');
      } else {
        drupal_set_message(t('Your new device has been created.'), 'status');
      }
    }
  }
  $form_state['redirect'] = array(
      'admin/config/sensors_devices',
      array(
        ),	
      );
}


<?php


function sensors_admin() {
  $form = array();

  $form['cost'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_cost_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Electricity Costs'),
      );

  $form['cost']['sensors_unit_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Electricity Rate per KWh'),
      '#default_value' => variable_get('sensors_unit_rate', 0.1457),
      '#size' => 7,
      '#maxlength' => 7,
      '#description' => t("The cost of electricity in KWh."),
      );
  $form['cost']['sensors_standing_charge'] = array(
      '#type' => 'textfield',
      '#title' => t('Electricity Daily Standing Charge'),
      '#default_value' => variable_get('sensors_standing_charge', 0.14),
      '#size' => 7,
      '#maxlength' => 7,
      '#description' => t("The standing charge cost of electricity per day."),
      );
  $form['goal'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_cost_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Sensor Goals'),
      );

  $form['goal']['sensors_power_goal'] = array(
      '#type' => 'textfield',
      '#title' => t('Electricity, Power goal'),
      '#default_value' => variable_get('sensors_power_goal', 250),
      '#size' => 7,
      '#maxlength' => 7,
      '#description' => t("Your ideal maximum power consumption in Watts."),
      );
  $form['goal']['sensors_temp_goal'] = array(
      '#type' => 'textfield',
      '#title' => t('Temperature, Deg C goal'),
      '#default_value' => variable_get('sensors_temp_goal', 20),
      '#size' => 7,
      '#maxlength' => 7,
      '#description' => t("Your ideal temperature in Deg C."),
      );
  $sensors_db = sensors_get_devices_sensors_db();
  $devices = array();
  $pwr_sensors = array();
  $temp_sensors = array();
  foreach($sensors_db as $sensor) {
    $devices[] = $sensor->deviceID . ':' . $sensor->name;
    if($sensor->typeID == 1) {
      /* Electricity sensor */
      $pwr_sensors[] = $sensor->sensorID . ':' . $sensor->s_name;
    } else if($sensor->typeID == 9) {
      /* Temp sensor */
      $temp_sensors[] = $sensor->sensorID . ':' . $sensor->s_name;
    }
  }
  $form['masters'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_master_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Master Devices & Sensors'),
      );
  $form['masters']['master_description'] = array(
      '#markup' => '<p>' 
      . t('The master device is the default device that is shown in blocks and on pages.')
      . '</p>',
      );
  $form['masters']['master_device'] = array(
      '#type' => 'select',
      '#title' => t('Master Device:'),
      '#options' =>  $devices,
      '#default_value' => variable_get('master_device', NULL),
      );

  return(system_settings_form($form));
}

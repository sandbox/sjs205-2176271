<?php



/**
 * Page to add sensor readings to database.  
 */
function sensors_upload_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_upload_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 *
 * GET query example:
 *  measurement[]=5&name[]=test&sensorID=1&measurement[]=6&name[]=test
 */
function sensors_upload_my_form($form_state) {

 $reading = array(
     'deviceID' => (isset($_REQUEST['deviceID']) ? $_REQUEST['deviceID'] : NULL),
     'date' => (isset($_REQUEST['date']) ? $_REQUEST['sensorID'] : date("Y-m-d H:i:s")),
     'measurements' => array(),
     );

  /* Should add authentication here!!!!!!!!!!!!!! */
  $elements = count((isset($_REQUEST['measurement']) ? $_REQUEST['measurement'] : NULL));
  for($i=0;$i < $elements;$i++) {
    $reading['measurements'][$i] = array(
        'measurement' => ( isset($_REQUEST['measurement'][$i]) ? $_REQUEST['measurement'][$i] : NULL)
        );
        $reading['measurements'][$i]['sensorID'] = (isset($_REQUEST['sensorID']) ? $_REQUEST['sensorID'] : NULL);
  }

  /* Start Page */

  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );

  $form['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  if( $reading['deviceID'] != NULL &&  isset($reading['measurements'][0])) {
    $form['contents'] = array(
        '#markup' => '<h3>DeviceID: <b>' . $reading['deviceID'] . ' </b></h3>' 
        . '<h4>Reading Date: <b>' . $reading['date'] . ' </b></h4>'
        );
    for($i=0;$i < $elements;$i++) {
      $form['measurements_' . $i] = array(
          '#markup' => '<h5>SensorID <b>' . $reading['measurements'][$i]['sensorID'][0] . ' </b>'
          . ' Measurement: <b>' . $reading['measurements'][$i]['measurement'] . ' </b></h5>'
          );
    } 
    $reading['readingID'] = sensors_insert_reading_db($reading['deviceID'],$reading['measurements'],$reading['date']);
      if($reading['readingID'] < 0) {
        drupal_set_message("The sensor measurement could not be inserted in to the database",
            'error');
      }else{
        drupal_set_message("The sensor measurement has been inserted in to the database",
            'status');
        $form['contents_readingID'] = array(
            '#markup' => '<h3>ReadingID: <b>' . $reading['readingID'] . ' </b></h3>' 
            );
      }

  } else {
    drupal_set_message("No sensor data is available, please try again", 'warning');

  }

  $form['row_container_1_end'] = array(
      '#markup' => '</div>'
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_profiles_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_profiles_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_profiles_my_form($form_state) {


  $deviceID = get_deviceID();

  $sensors = get_device_master_sensors($deviceID);
  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );


  $form['profiles'] = profile_list_block();

  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Create Sensor Profile'),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  /* Device/Sensor selection. */
  $devices = get_device_list();
  $form['fieldset_create']['device'] = array(
      '#prefix' => '<p>' . t('Select a device and sensor below and start creating the a new sensor profile.') . '</p>',
      '#type' => 'select',
      '#title' => t('Device:'),
      '#options' =>  $devices,
      '#default_value' => $deviceID,
      '#attributes' => array('onchange' => "form.submit('device_select')"),
      );
  /* hidden submit button - required for onchange attribute*/
  $form['fieldset_create']['device_select'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
      '#submit' => array('sensors_profiles_update_device_submit'),
      '#attributes' => array('style' => 'display:none;'),
      );
  if ($deviceID) {
    $form['fieldset_create']['sensors'] = sensors_list_block($deviceID,
        NULL, 1);
  } else {
    $form['fieldset_create']['sensors'] = NULL;
  }

  $form['fieldset_create']['sensors']['#prefix'] = '<div id="sensors_sensors_table">'; 
  $form['fieldset_create']['sensors']['#suffix'] = '</div>'; 

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['profile_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_profile_name_desc">',
      '#suffix' => '</div>',
      );
  $form['fieldset_create']['profile_name_desc']['profile_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Profile Name'),
      '#size' => 60, 
      '#maxlength' => 32, 
      );
  $form['fieldset_create']['profile_name_desc']['profile_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Profile Description'),
      '#size' => 60, 
      '#maxlength' => 256, 
      );

  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );

  $form['fieldset_create']['row_container_3'] = array(
      '#markup' => '<div id="sensors_row_3">'
      );
  $form['fieldset_create']['row_dates'] = array(
      '#markup' => '<div id="sensors_dates">'
      );

  $format = 'Y-m-d H:i:s';
  $form['fieldset_create']['start_date_fieldset'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_profile_start_date">',
      '#suffix' => '</div>',
      );


  $form['fieldset_create']['start_date_fieldset']['profile_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Start Date'),
      '#date_format' => $format,
      '#date_label_position' => 'above',
      '#date_increment' => 1, /*to increment minutes */
      '#date_year_range' => '-25:+25',
      '#default_value' => NULL,
      '#prefix' => '<div id="sensors_profile_start_date_internal">',
      );

  $form['fieldset_create']['start_date_fieldset']['end_date_later'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set End Date Later'),
      '#default_value' => TRUE,
      '#suffix' => '</div>',
      );

  $form['fieldset_create']['end_date_fieldset'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_profile_end_date">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          ':input[name="end_date_later"]' => array('checked' => FALSE)),
        ),
      );
  $form['fieldset_create']['end_date_fieldset']['profile_end_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('End Date'),
      '#date_format' => $format,
      '#date_label_position' => 'above',
      '#date_increment' => 1, /*to increment minutes */
      '#date_year_range' => '-25:+25',
      '#default_value' => NULL,
      );
  $form['fieldset_create']['row_dates_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_3_end'] = array(
      '#markup' => '</div>',
      );

  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => 'Submit',
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_profiles_my_form_submit'),
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

/* 
 * sensors_sensor_profiles_my_form submit handler
 */
function sensors_profiles_update_device_submit($form, &$form_state) {
  $form_state['redirect'] = array(
      'sensors_profiles',
      array(
        'query' => array(
          'deviceID' => $form_state['values']['device'],),
        ),
      );
}

function sensors_profiles_my_form_submit($form, &$form_state) {
  if(!(isset($form_state['values']['sensors_tableselect']) &&
        isset($form_state['values']['profile_name']) &&
        isset($form_state['values']['profile_description']) &&
        isset($form_state['values']['profile_date']))) {
    drupal_set_message("You must specify at least 'Profile Name', 'Profile Description'"
        . " and 'Start Date' to create a new profile.", 'warning');
  } else {
    $tmp = sensors_insert_profile_db(
        $form_state['values']['sensors_tableselect'],
        $form_state['values']['profile_name'],
        $form_state['values']['profile_description'],
        $form_state['values']['profile_date'],
        $form_state['values']['profile_end_date']);

    if($tmp < 0) {
      drupal_set_message(t('New profile could not be created...'), 'warning');
    } else {
      drupal_set_message(t('Your new profile has been created.'), 'status');
    }
  }

  $form_state['redirect'] = array(
      'sensors_profiles',
      array(
        'query' => array(
          'deviceID' => $form_state['values']['device'],),
        ),	
      );
}


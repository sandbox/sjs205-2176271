<?php


function sensors_profile_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_profile_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_profile_my_form($form_state) {

  $form = array();

  $profile = array();
  if(isset($_REQUEST['profileID'])) {
    $profile['ID'] = $_REQUEST['profileID'];
  }

  $profile = get_profile($profile['ID']);
  if(isset($_REQUEST['deviceID'])) {
    $deviceID = $_REQUEST['deviceID'];
  } else {
    $deviceID = $profile['sensor']['deviceID'];
  }
  $devices = get_device_list();

  /* Calculate interval */
  $profile['interval'] = get_dates_interval_minutes($profile['date'],
      isset($profile['end_date']) ? $profile['end_date'] : NULL);
  /* get profile reading */
  $profile['readings_db'] = sensors_get_profile_readings_db($profile['ID']);

  $profile['maxmin'] = get_readings_maxmin($profile['readings_db']);

  /* Start Page */

  $form['page_container'] = array(
      '#markup' => '<div id="sensorscontainer">'
      . '<h2>' . $profile['name'] . ' - ' . $profile['description'] . '</h2>'
      );


  $form['row_container_1'] = array(
      '#markup' => '<div id="sensorsrow_1">'
      );

  $form['profile_stats'] = element_wrapper(
      profile_stats_block($profile),
      'Sensor Profile',
      'profile_stats',
      1);
  $form['sensor_stats'] = element_wrapper(
      sensor_stats_block($profile['sensor']),
      'Sensor',
      'sensor_stats',
      2);
  $form['row_container_1_end'] = array(
      '#markup' => '</div>'
      );

  $form['row_container_2'] = array(
      '#markup' => '<div id="sensorsrow_2">'
      );

  $profile['measurements'] = calc_average_reading_set($profile['readings_db'],
      $profile['interval'], $profile['date']
      );
  $title_lasth_chart = $profile['name'] . ' (' .  $profile['sensor']['units'] . ')';
  /*$form['profile_chart'] = element_wrapper(
      render_last_nh_reading($profile['interval'], $title_lasth_chart,$profile['measurements'],650),
      'Profile Last ' . $profile['interval'] . ' Minutes',
      'profile_chart',
      'x');
*/
      $form['row_container_2_end'] = array(
        '#markup' => '</div><div class="hrule"><hr></div>'
        );

      /* Device/Sensor selection. */
      $form['fieldset'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="sensorsfieldset_change_profile">',
        '#suffix' => '</div>',
        '#title' => t('Change profile'),
        );  
      $form['fieldset']['device'] = array(
        '#prefix' => '<p>' . t('Select a device or a profile below to change the current profile.') . '</p>',
        '#type' => 'select',
        '#title' => t('Device:'),
        '#options' =>  $devices,
        '#default_value' => $deviceID,
        '#attributes' => array('onchange' => "form.submit('results')"),
        );
      $form['fieldset']['sensors'] = profile_list_block($deviceID);

      $form['page_container_end'] = array(
          '#markup' => '</div>'
          );

      return $form;
}

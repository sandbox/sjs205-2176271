<?php



/**
 * Page to display last weeks daily averages 
 */
function sensors_sensor_types_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'sensors') . '/sensors.css');

  return drupal_get_form('sensors_sensor_types_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function sensors_sensor_types_admin($form_state) {


  $form_state['values']['typeID'] = (isset($_REQUEST['typeID']) ? $_REQUEST['typeID'] : NULL);
  $update = FALSE;
  if($form_state['values']['typeID'] != NULL) {
    /* UPDATE query */
    $update = TRUE;
    $sensor_type = sensors_get_sensor_type_db($form_state['values']['typeID']);
  }

  $sensor_types = get_sensor_types_list();
  /* DO TYPE STUFF HERE *?

  /* Start Page */
  
  $form['page_container'] = array(
      '#markup' => '<div id="sensors_container">'
      );
  $form['fieldset_sensors'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Available Sensor Types'),
      );


  $form['fieldset_sensors']['sensor_types'] = sensor_types_list_block(NULL, 'admin/config/sensor_types');
  $form['fieldset_create'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Create New Sensor'),
      );
  $form['fieldset_create']['row_container_1'] = array(
      '#markup' => '<div id="sensors_row_1">'
      );

  $form['fieldset_create']['sensor_help'] = array(
      '#markup' => '<p>' .
      ($update 
       ? t('Update sensor type details below, once complete, press \'Submit\'.') 
       : t('Enter details of the new sensor type to be added below, once complete, press Submit.') 
      ) . '</p>',
      );

  $form['fieldset_create']['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['fieldset_create']['row_container_2'] = array(
      '#markup' => '<div id="sensors_row_2">'
      );
  $form['fieldset_create']['sensor_type_name_desc'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="sensors_sensor_type_name_desc">',
      '#suffix' => '</div>',
      );

  if($update) {
    $form['fieldset_create']['sensor_type_name_desc']['typeID'] = array(
        '#type' => 'textfield',
        '#title' => t('typeID'),
        '#size' => 60, 
        '#maxlength' => 32, 
        '#default_value' => $sensor_type['typeID'],
        '#attributes' => array('disabled' => 'TRUE'),
        );
  }
  $form['fieldset_create']['sensor_type_name_desc']['sensor_type_type'] = array(
      '#type' => 'textfield',
      '#title' => t('Sensor Type'),
      '#size' => 60, 
      '#maxlength' => 32, 
      '#default_value' => ($update ? $sensor_type['type'] : NULL ),
      );
  $form['fieldset_create']['sensor_type_name_desc']['sensor_type_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Sensor Description'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $sensor_type['description'] : NULL ),
      );
  $form['fieldset_create']['sensor_type_name_desc']['sensor_type_units'] = array(
      '#type' => 'textfield',
      '#title' => t('Units'),
      '#size' => 60, 
      '#maxlength' => 256, 
      '#default_value' => ($update ? $sensor_type['units'] : NULL ),
      );
  $form['fieldset_create']['row_container_2_end'] = array(
      '#markup' => '</div>'
      );


  $form['fieldset_create']['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Create Profile'),
      '#value' => 'Submit',
      '#prefix' => '<div class="sensors_submit">',
      '#suffix' => '</div>',
      '#submit' => array('sensors_sensor_types_my_form_submit'),
      );

  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );
  return $form;
}

function sensors_sensor_types_my_form_submit($form, &$form_state) {

  if(isset($form_state['values']['typeID'])) {
    /* UPDATE existing Device */
    if(!(isset($form_state['values']['sensor_type_type']) &&
          isset($form_state['values']['sensor_type_description']) &&
          isset($form_state['values']['sensor_type_units']))) {
      drupal_set_message("You must specify at least 'Sensor Type', 'Sensor Type Description'"
          . " and Sensor Type Units to update a sensor type.", 'warning');
    } else {
      $tmp = sensors_update_sensor_type_db(
          $form_state['values']['typeID'],
          $form_state['values']['sensor_type_type'],
          $form_state['values']['sensor_type_description'],
          $form_state['values']['sensor_type_units']);

      if($tmp < 0) {
        drupal_set_message(t('Your sensor type could not be updated...'), 'warning');
      } else {
        drupal_set_message(t('Your sensor type has been updated.'), 'status');
      }
    }
  } else {
    $tmp = 0;

    if(!(isset($form_state['values']['sensor_type_type']) &&
          isset($form_state['values']['sensor_type_description']) &&
          isset($form_state['values']['sensor_type_units']))) {
      drupal_set_message("You must specify at least 'Sensor Type', 'Sensor Type Description'"
          . " and Sensor Type Units to create a new sensor type.", 'warning');
    } else {
      $tmp = sensors_insert_sensor_type_db(
          $form_state['values']['sensor_type_type'],
          $form_state['values']['sensor_type_description'],
          $form_state['values']['sensor_type_units']);

      if($tmp < 0) {
        drupal_set_message(t('New sensor type could not be created...'), 'warning');
      } else {
        drupal_set_message(t('Your new sensor type has been created.'), 'status');
      }
    }
  }
  $form_state['redirect'] = array(
      'admin/config/sensor_types',
      array(
        ),	
      );
}


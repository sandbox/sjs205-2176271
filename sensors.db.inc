<?php
/* 
 * Fuction to return a list of devices with attached sensors
 * If deviceID is provide the function will only return sensors connected 
 * to this given device
 */
function sensors_get_devices_sensors_db($deviceID = NULL, $sensorID = NULL) {
  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor', 's');
  $query->join('sensors_device', 'd', 
      'd.deviceID = s.deviceID');
  $query->join('sensors_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('d', array('deviceID','name','description'));
  $query->fields('s', array('sensorID','name','typeID', 'master', 'description', 'hardwareID'));
  $query->fields('t', array('type', 'description','units'));
  if($deviceID != NULL) {
    $query->condition('d.deviceID', $deviceID);
  }
  if($sensorID != NULL) {
    $query->condition('s.sensorID', $sensorID);
  }

  $result = $query->execute()->fetchAllAssoc('sensorID');

  db_set_active();
  return $result;
}

/* 
 * Fuction to return a list of devices with attached profiles
 * If deviceID is provide the function will only return profiles connected 
 * to this given device
 */
function sensors_get_devices_profiles_db($deviceID = NULL, $sensorID = NULL) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor_profile', 'p');
  $query->join('sensors_sensor', 's', 
      's.sensorID = p.sensorID');
  $query->join('sensors_device', 'd', 
      'd.deviceID = s.deviceID');
  $query->join('sensors_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('s', array('sensorID','name', 'typeID', 'master'));
  $query->fields('t', array('type', 'description','units'));
  $query->fields('p', array('profileID', 'name','description','date','end_date'));
  if($deviceID) {
    $query->condition('d.deviceID', $deviceID);
  }
  if($sensorID) {
    $query->condition('s.sensorID', $sensorID);
  }
  $result = $query->execute()->fetchAllAssoc('profileID');


  db_set_active();
  return $result;

}

/*
 * Function to return readings related to a profile.
 *  if $end_date is null then it is assumed that an
 *  end_date field is not present and it will return all
 *  dates up to now. - needs a better implimentation...
 */
function sensors_get_profile_readings_db($profileID) {
  db_set_active('sensors_live_db');
  $query_date_test = db_select('sensors_sensor_profile', 'p');
  $query_date_test->condition('p.profileID', $profileID);
  $query_date_test->fields('p', array('end_date'));
  $result_date_test = $query_date_test->execute()->fetchAssoc();
  
  $query = db_select('sensors_measurement', 'm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = m.readingID');
  $query->join('sensors_sensor', 's', 
      'm.sensorID = s.sensorID');
  $query->join('sensors_sensor_profile', 'p', 
      's.sensorID = p.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','ASC');
  $query->condition('p.profileID', $profileID);
  if($result_date_test['end_date'] == NULL) {
    $query->where('r.date BETWEEN p.date AND NOW()');
  } else {
    $query->where('r.date BETWEEN p.date AND p.end_date');
  }
  $result = $query->execute()->fetchAllAssoc('date');

  db_set_active();
  return($result);

}



/* 
 * Fuction to return a sensor's data
 */
function sensors_get_sensor_db($sensorID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor', 's');
  $query->join('sensors_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('t', array('units','type','description'));
  $query->fields('s', array('sensorID','deviceID','name','description', 'typeID', 'master', 'hardwareID'));
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();


  db_set_active();
  return $result;

}

/* 
 * Fuction to return a sensor's calculations
 */
function sensors_get_sensor_calcs_db($sensorID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor_calc', 'c');
  $query->fields('c', array('calc_type', 'date', 'time_interval', 'expires', 'result'));
  $query->condition('c.sensorID', $sensorID);
  $query->orderBy('c.expires','DESC')->range(0,12);
  $result = $query->execute()->fetchAll();;

  db_set_active();
  return $result;

}

/* 
 * Fuction to return a sensor types data
 */
function sensors_get_sensor_types_db() {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor_type', 't');
  $query->fields('t', array('typeID','units','type','description'));
  $result = $query->execute()->fetchAllAssoc('typeID');


  db_set_active();
  return $result;

}

function sensors_get_sensor_type_db($typeID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor_type', 't');
  $query->fields('t', array('typeID','units','type','description'));
  $query->condition('t.typeID', $typeID);
  $result = $query->execute()->fetchAssoc();


  db_set_active();
  return $result;
}

/* 
 * Fuction to return a profile's data
 */
function sensors_get_profile_db($profileID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_sensor_profile', 'p');
  $query->join('sensors_sensor', 's', 
      's.sensorID = p.sensorID');
  $query->join('sensors_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('p', array('name','description','date','end_date'));
  $query->fields('t', array('units','type','description'));
  $query->fields('s', array('sensorID','deviceID','name','description','typeID', 'master'));
  $query->condition('p.profileID', $profileID);
  $result = $query->execute()->fetchAssoc();


  db_set_active();
  return $result;

}

/* 
 * Fuction to return a list of devices
 */
function sensors_get_devices_db() {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_device', 'd');
  $query->fields('d', array('deviceID','name','description'));
  $result = $query->execute()->fetchAllAssoc('deviceID');


  db_set_active();
  return $result;

}

/* 
 * Fuction to return a device
 */
function sensors_get_device_db($deviceID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_device', 'd');
  $query->fields('d', array('deviceID','name','description','sver'));
  $query->condition('d.deviceID', $deviceID);
  $result = $query->execute()->fetchAssoc();


  db_set_active();
  return $result;

}

/* 
 * Fuction to return a current reading from a sensorID
 */
function sensors_get_reading_db($sensorID, $date = NULL) {

  db_set_active('sensors_live_db');
  $query = db_select('sensors_reading', 'r');
  $query->join('sensors_measurement', 'm', 
      'r.readingID = m.readingID');
  $query->join('sensors_sensor', 's', 
      'm.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','DESC')->range(0,1);
  if($date != NULL) {
    $query->where('r.date <= \'' . $date . '\'' );
  }
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();


  db_set_active();
  return $result;

}

/* 
 * Fuction to insert a current reading 
 */
function sensors_insert_reading_db($deviceID, $measurements, $date = NULL) {

  db_set_active('sensors_live_db');
  $readingID = db_insert('sensors_reading')
    ->fields(array(
          'deviceID' => $deviceID,	
          'date' => ($date == NULL ? date("Y-m-d H:i:s") : $date),
          ))
    ->execute();

  foreach($measurements as $measurement) {
    $tmp = db_insert('sensors_measurement')
      ->fields(array(
            'readingID' => $readingID,
            'sensorID' => $measurement['sensorID'],
            'measurement' => $measurement['measurement'],
            ))->execute();
  }

  db_set_active();
  return($readingID);
}

/* 
 * Fuction to return readings from a deviceID
 */
function device_get_sensors_readings_db($deviceID, $count = 20) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_reading', 'r');
  $query->join('sensors_measurement', 'm', 
      'r.readingID = m.readingID');
  $query->join('sensors_sensor', 's', 
      'm.sensorID = s.sensorID');
  $query->join('sensors_device', 'd', 
      'r.deviceID = d.deviceID');
  $query->join('sensors_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('m', array('measurement'));
  $query->fields('r', array('readingID', 'date'));
  $query->fields('s', array('sensorID', 'description', 'name')); 
  $query->fields('t', array('units', 'type')); 
  $query->condition('r.deviceID', $deviceID);
  $query->orderBy('r.date','DESC')->range(0,$count);
  $result = $query->execute()->fetchAll();

  db_set_active();
  return $result;
}

/* 
 * Function to return readings from a sensorID
 */
function sensors_get_sensor_readings_db($sensorID) {

  db_set_active('sensors_live_db');

  $query = db_select('sensors_measurement', 'm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = m.readingID');
  $query->join('sensors_sensor', 's', 
      'm.sensorID = s.sensorID');
  $query->fields('m', array('measurement'));
  $query->fields('r', array('readingID', 'date'));
  $query->condition('m.sensorID', $sensorID);
  $query->orderBy('r.date','DESC')->range(0,10);
  $result = $query->execute()->fetchAllAssoc('readingID');

  db_set_active();
  return $result;

}

function sensors_get_last_nhour_measurements_db($sensorID, $hours = NULL, $calc=NULL) {
  db_set_active('sensors_live_db');
  $query = db_select('sensors_measurement', 'm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = m.readingID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','ASC');
  if(isset($hours)) {
    $query->where('r.date >= DATE_SUB(NOW(), INTERVAL ' . $hours . ' HOUR)');
  }
  $query->condition('m.sensorID', $sensorID);
  if(isset($calc)) {
    switch($calc) {
      case 'AVG':
        $query->addExpression('AVG(CAST(measurement AS DECIMAL ))');
        break;
      case 'MIN':
        $query->addExpression('MIN(CAST(measurement AS DECIMAL ))');
        break;
      case 'MAX':
        $query->addExpression('MAX(CAST(measurement AS DECIMAL ))');
        break;
      case 'SUM':
        $query->addExpression('SUM(CAST(measurement AS DECIMAL ))');
        break;
    }
  }
  $result = $query->execute()->fetchAllAssoc('date');

  db_set_active();
  return($result);

}


function sensors_get_date_range_measurements_db($sensorID, $start_date = NULL, 
    $end_date = NULL, $calc=NULL) {

  db_set_active('sensors_live_db');
  $query = db_select('sensors_measurement', 'm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = m.readingID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','ASC');
  if(isset($start_date) && isset($end_date)) {
    $query->where('r.date BETWEEN \'' .  $start_date . '\''
        . ' AND \'' . $end_date . '\'');
  }
  $query->condition('m.sensorID', $sensorID);
  switch($calc) {
    case 'AVG':
      $query->addExpression('AVG(CAST(measurement AS DECIMAL ))');
      break;
    case 'MIN':
      $query->addExpression('MIN(CAST(measurement AS DECIMAL ))');
      break;
    case 'MAX':
      $query->addExpression('MAX(CAST(measurement AS DECIMAL ))');
      break;
    case 'SUM':
      $query->addExpression('SUM(CAST(measurement AS DECIMAL ))');
      break;
  }
  $result = $query->execute()->fetchAllAssoc('date');

  db_set_active();
  return($result);

}

function sensors_get_date_avg_db($sensorID, $date) {
  /* $date should be in mysql 'date' format. YYYY-MM-DD */
  db_set_active('sensors_live_db');
  $query = db_select('sensors_measurement', 'tm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = tm.readingID');
  $query->join('sensors_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->addExpression('AVG(measurement)');
  $query->where('r.date BETWEEN \'' .  $date . ' 00:00:00\''
      . ' AND DATE_ADD(\'' . $date . ' 00:00:00\', INTERVAL 1 DAY)');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  db_set_active();
  return($result);
}

/* 
 * function returns average for a measument for a particular day, $date, at a particuar 
 * date duration, $date_end - $date_start 
 */
function sensors_get_interval_avg_db($sensorID, $date_start, $date_end) {
  /* $date should be in mysql 'datetime' format. YYYY-MM-DD */
  db_set_active('sensors_live_db');
  $query = db_select('sensors_measurement', 'tm');
  $query->join('sensors_reading', 'r', 
      'r.readingID = tm.readingID');
  $query->join('sensors_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->addExpression('AVG(measurement)');
  $query->where('r.date BETWEEN \'' .  $date_start . '\''
      . ' AND \'' . $date_end . '\'');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  db_set_active();
  return($result);
}


function sensors_get_sensor_calc_db($sensorID, $calctype, $date, $interval) {
  db_set_active('sensors_live_db');
  $query = db_select('sensors_sensor_calc', 'c');
  $query->where('c.date BETWEEN \'' .  $date . ' 00:00:00\''
      . ' AND DATE_ADD(\'' . $date . ' 00:00:00\', INTERVAL 1 DAY)');
  $query->condition('c.sensorID', $sensorID);
  $query->condition('c.calc_type', $calctype);
  $query->condition('c.time_interval', $interval);
  $query->where('c.expires >= NOW() OR c.expires IS NULL');
  $query->fields('c', array('result'));
  $result = $query->execute()->fetchAssoc();

  db_set_active();
  return($result);
}

/* INSERT QUERIES */

function sensors_insert_device_db($name, $description, $sver = NULL) {
  db_set_active('sensors_live_db');
  $tmp = db_insert('sensors_device')
    ->fields(array(
          'sver' => $sver,	
          'name' => $name,
          'description' => $description,
          ))
    ->execute();

  db_set_active();
  return($tmp);
}

function sensors_insert_sensor_db($deviceID, $name, $description, $typeID, $hardwareID = NULL) {
  db_set_active('sensors_live_db');
  $tmp = db_insert('sensors_sensor')
    ->fields(array(
          'deviceID' => $deviceID,
          'typeID' => $typeID,
          'hardwareID' => $hardwareID,	
          'name' => $name,
          'description' => $description,
          ))
    ->execute();

  db_set_active();
  return($tmp);
}

function sensors_insert_sensor_type_db($type, $description, $units) {
  db_set_active('sensors_live_db');
  $tmp = db_insert('sensors_sensor_type')
    ->fields(array(
          'units' => $units,	
          'type' => $type,
          'description' => $description,
          ))
    ->execute();

  db_set_active();
  return($tmp);
}

function sensors_insert_sensor_calc_db($sensorID, $calctype, $date, $interval, $result, $expires = NULL) {
  db_set_active('sensors_live_db');
  if($sensorID != 0) {
    $tmp = db_insert('sensors_sensor_calc')
      ->fields(array(
            'sensorID' => $sensorID,	
            'calc_type' => $calctype,
            'date' => $date,
            'time_interval' => $interval,
            'result' => $result,
            'expires' => $expires,
            ))
      ->execute();

  db_set_active();
    return($tmp);
  }

  db_set_active();
  return(-1);
}

function sensors_insert_profile_db($sensorID, $name, $description, $date, $end_date = NULL) {
  db_set_active('sensors_live_db');
  if($sensorID != 0) {
    $tmp = db_insert('sensors_sensor_profile')
      ->fields(array(
            'sensorID' => $sensorID,	
            'name' => $name,
            'description' => $description,
            'date' => $date,
            'end_date' => $end_date,
            ))
      ->execute();

  db_set_active();
    return($tmp);
  }

  db_set_active();
  return(-1);
}

/* UPDATE Queries */

function sensors_update_device_db($deviceID, $name, $description, $sver = NULL) {
  db_set_active('sensors_live_db');
  $tmp = db_update('sensors_device')
    ->fields(array(
          'sver' => $sver,	
          'name' => $name,
          'description' => $description,
          ))
    ->condition('deviceID', $deviceID)
    ->execute();

  db_set_active();
  return($tmp);
}

function sensors_update_sensor_db($sensorID, $deviceID, $name, $description, $typeID, $hardwareID = NULL) {
  db_set_active('sensors_live_db');
  $tmp = db_update('sensors_sensor')
    ->fields(array(
          'deviceID' => $deviceID,
          'typeID' => $typeID,
          'hardwareID' => $hardwareID,	
          'name' => $name,
          'description' => $description,
          ))
    ->condition('sensorID', $sensorID)
    ->execute();

  db_set_active();
  return($tmp);
}

function sensors_update_sensor_type_db($typeID, $type, $description, $units) {
  db_set_active('sensors_live_db');
  $tmp = db_update('sensors_sensor_type')
    ->fields(array(
          'units' => $units,	
          'type' => $type,
          'description' => $description,
          ))
    ->condition('typeID', $typeID)
    ->execute();

  db_set_active();
  return($tmp);
}

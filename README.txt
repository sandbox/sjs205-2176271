sensors 
=======

CONTENTS OF THIS FILE 
---------------------
 * Introduction 
 * Requirements 
 * Installation 
 * Adding new sensors 
 * Uploading sensor data
 * Troubleshooting 
 * FAQ 
 * Maintainers


INTRODUCTION 
------------ 
The sensors module provides a basic framework for managing devices, sensors and 
sensor data. 


REQUIREMENTS 
------------ 
This module requires the following modules:
 * Views (https://drupal.org/project/views) Views megarow
 * (https://drupal.org/project/views_megarow) Date
 * (https://drupal.org/project/date)


INSTALLATION 
------------ 
 1) Install sensors by either unpacking it to your
    Drupal /sites/all/modules directory if you're installing by hand, or using the

    /admin/modules/install page.

 2) Enable sensors module in /admin/modules.


ADDING NEW SENSORS 
------------------ 
 1) Add a device: Before a new sensor can
    be added, a device must exist that the sensor can be attached to. This is done
    through the admin page:

    /admin/config/sensors_devices

 2) The sensor must also have a sensor_type in place too. 

    /admin/config/sensor_types

 3) Once both device and sensor_type has been created, it is now time to add a
    sensor, this can be done one the following page: 

    /admin/config/sensors_sensors

That is it, you're now ready to start uploading sensor data. 


UPLOADING SENSOR DATA 
--------------------- 
At present there is no official way
to upload sensor data, although this process is likely to cange in the very near
future. 

One possible implementation that is currently in place is the sensor_upload
page. This allows a number of sensor measuments to be uploaded for multiple
sensors connected to a single device instance. This function will be changed or
removed in time since it is currenly insecure. the request takes the following
form:

http://www.test.com/?q=sensors_upload&deviceID=1&measurement[]=2.5&&sensorID[]=2&measurement[]=test_data&&sensorID[]=3

The above request creates a single reading and attaches the measurements from 2
sensors to it. Both sensors should be attached to the given device. 

Sensors measurements are stored in the database as text and then converted to
numeric formats as required. 


TROUBLESHOOTING 
---------------


FAQ 
--- 
No questions yet, ASK AWAY! 


MAINTAINERS 
----------- 
Current maintainers:
 * Steven Swann (sjs205) https://drupal.org/user/2256618


Further information can be found on the project page. 
